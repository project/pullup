README
======

Use field data to add classes to parent bundle (taxonomy terms on Article nodes, for example).

Optionnally generate css file if terms have a color field
(jquery_colorpicker_ and colorpicker modules supported)

* Go to /admin/structure/pullup to configure and refresh css file when applicable.
* Select vocabularies to use on each node type.
* Select color field machine name and colorpicker module configuration.

jquery_colorpicker_ and colorpicker modules both provide an RGB color field.

Goto /admin/structure/taxonomy/tags/fields
Add a "jQuery Colorpicker" or "Color Picker (RGB)" field to the Tags vocabulary.
Give it the machine name "color" (-> field_color)_

Goto /admin/structure/taxonomy/tags/add to add a new term (Foobar)
(or edit a term you already have) and make sure you give it a color.

Goto /node/add/article and Create a new article. Make sure to use the "Foobar" tag.

After creation, the node should appear normally (node/123)

Goto /admin/structure/pullup to configure and refresh css file when applicable.
Under "Select which fields to "Pull Up" for Article content type",
select the Tags field as defined by the standard Drupal 7 install profile.

Under "color field", enter "field_color"_
Under "module config", enter "jquery_colorpicker"_ or "rgb" for the colorpicker module support

Hit "Save configuration"

This should create the file DRUPAL/sites/sitename/files/css/pullup.css

Goto /node/123 (the node you just created) and it should now have the color you gave its term.
Note that the div element for the whole node has an extra class (or more),
identifying each term and the vocab they belong to.

It should look a bit like this:

/* pullup generated css file */
.vid-1-tid-8 { border: 10px solid  #f70808; padding: -10px; }
.vid-1-tid-5 { border: 10px solid  #00eb10; padding: -10px; }


Todo
----
* Improve project page
* Make translatable
* Open a few issues: roadmap, some of these todos, etc.
* Integrate with http://drupal.org/project/variable http://drupal.org/node/1697868
* Blindly setting background in pullup.css is a little dumb... How to improve that without going overboard? http://drupal.org/node/1697872
* Replace both textfield configurations with select lists and/or automatically
* pullup.install: take care of remaining variables
* Move or replicate (<--preferred) most config to content type config form
* Write post on pullup module use cases http://rym.waglo.com/en/node/248
* Features/Ctools export for config?
* Generalize to support other entities (currently only Nodes are supported)
* Generalize to support other references (currently only Taxonomy_term_ references are supported)
* Should we somehow support the core color module?
* Use machine names instead of vid and tid in class name generation
