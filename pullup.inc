<?php

function pullup_settings() {
  // strange hack to save a bit of db space; for more info
  // see http://drupal.org/node/61760
  $form = array('array_filter' => array('#type' => 'hidden'));
  foreach (_pullup_supported() as $bundle => $stuff) {
    $form['pullup_settings']['pullup_bundle_' . $bundle] = array(
      '#type' => 'checkboxes',
      '#options' => $stuff['fields'],
      '#default_value' => variable_get('pullup_bundle_' . $bundle, array()),
      '#title' => "Select which fields to \"Pull Up\" for {$stuff['label']} content type:",
    );
  }

  $form['pullup_settings']['pullup_color_field'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('pullup_color_field', 'field_color'),
    '#title' => 'color field',
    '#description' => 'Name of the color field.',
  );

  $form['pullup_settings']['pullup_color_module'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('pullup_color_module'),
    '#title' => 'module config',
    '#description' => 'Set to "jquery_colorpicker" or "rgb" for colorfield module support.',
  );

  $form = system_settings_form($form);
  // add our submit handler AFTER the default one
  $form['#submit'][] = 'pullup_settings_form_submit';
  return $form;
}

function pullup_settings_form_submit($form, &$form_state) {
  $color_field = variable_get('pullup_color_field');
  $color_module = variable_get('pullup_color_module');
  $css = "/* pullup generated css file */\n";

  if (empty($color_field) || empty($color_module)) {
    // reinitialize the pullup css file, no styles
    _pullup_save_stylesheet($css);
    return;
  }

  $vocabularies = array();
  $knowns = array();
  foreach ($form_state['values'] as $bundle => $fields) {
    if (!is_array($fields)) continue;
    foreach (array_filter($fields) as $field) {
      if (isset($knowns[$field])) continue;
      $knowns[$field] = field_info_field($field);
      $voc = taxonomy_vocabulary_machine_name_load(
        $knowns[$field]['settings']['allowed_values'][0]['vocabulary']);
      $vocabularies[$voc->vid] = taxonomy_get_tree($voc->vid, 0, NULL, true);
    }
  }

  foreach ($vocabularies as $vid => $terms) {
    if (!count($terms)) continue;
    foreach ($terms as $term) {
      if (isset($term->{$color_field}['und'][0][$color_module])) {
        $color = ltrim($term->{$color_field}['und'][0][$color_module], '#');
        $css .= ".vid-$vid-tid-{$term->tid} {";
        $css .= " border: 10px solid  #$color; padding: -10px; }\n";
      }
    }
  }
  $css .= "\n";
  _pullup_save_stylesheet($css);
}

function _pullup_save_stylesheet($css) {
  $file = 'public://css/pullup.css';
  if (file_unmanaged_save_data($css, $file, FILE_EXISTS_REPLACE)) {
    drupal_chmod($file);
  }// else dpm('error saving css file');
}
